package com.example.observersexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lifecycle.addObserver(Observer())

        Log.d("Main" , "Activity OnCreate")
    }

    override fun onResume() {
        super.onResume()
        Log.d("Main" , "Activity OnCreate")
    }

    override fun onPause() {
        super.onPause()
        Log.d("Main" , "Activity onPause")
    }

    override fun onStart() {
        super.onStart()
        Log.d("Main" , "Activity onStart")
    }

    override fun onStop() {
        super.onStop()
        Log.d("Main" , "Activity onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Main" , "Activity onDestroy")
    }

}